package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.time.Duration;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(DemoApplication.class, args);

		Flux<Long> flux = Flux
				.interval(Duration.ofMillis(1000))
				.publish()
				.refCount(3);	// => publish().refCount() est équivalent à share()

		flux.subscribe(duration -> System.out.println("clock1 " + duration + "s"));

		Thread.sleep(2000);	// Simulate async call to webService

		flux.subscribe(duration -> System.out.println("\tclock2 " + duration + "s"));
		flux.subscribe(duration -> System.out.println("\tclock3 " + duration + "s"));
	}

}
