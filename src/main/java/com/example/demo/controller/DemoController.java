package com.example.demo.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;

@RestController
@RequestMapping("/demo")
public class DemoController {

    private final Flux<Long> fluxMinSubscriber = Flux
            .interval(Duration.ofMillis(1000))
            .publish()
            .refCount(2);   // Exige minimum 2 subscribers

    private final Flux<Long> sharedFlux = Flux
            .interval(Duration.ofMillis(1000))
            .share();

    @GetMapping(path = "minSubscriber", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Long> getIntervalWithMinSubscriber() {
        return fluxMinSubscriber;
    }

    @GetMapping(path = "shareStreamSSE", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Long> getSharedIntervalSSE() {
        return sharedFlux;
    }

    @GetMapping(path = "shareStream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Long> getSharedInterval() {
        return sharedFlux;
    }
}
